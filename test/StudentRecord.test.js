//Import the StudentList smart contract
const StudentRecord = artifacts.require('StudentRecord')
 
//Use the contract  to write all tests
//variable: account => all accounts in blockchain
contract('StudentRecord', (accounts) => {
    //Make sure contract is deployed and before
    //we retrieve the studentrecord object for testing
    beforeEach(async () => {
        this.studentRecord = await StudentRecord.deployed()
    })
 
    //Testing the deployed student contract
    it('Deploys successfully', async () => {
        //Get the address which the student object is stored
        const address = await this.studentRecord.address
        //Test for valid address
        isValidAddress(address)
    })

    //Testing the content in the contract
    it('Test adding students', async () => {
        return StudentRecord.deployed().then((instance) => {
        studentrecordInstance = instance;
        studentCid = 1;
        return studentrecordInstance.addStudent(studentCid, "Pema Yangzom");
        }).then((transaction) => {
        isValidAddress(transaction.tx)
        isValidAddress(transaction.receipt.blockHash);
        return studentrecordInstance.studentsCount()
        }).then((count) => {
        assert.equal(count, 1)
        return studentrecordInstance.students(1);
        }).then((student) => {
        assert.equal(student.cid, studentCid)
        })
    })

    it('Test finding students', async () => {
        return StudentRecord.deployed().then(async (instance) => {
            s = instance;
            studentCid = 2;
            return s.addStudent(studentCid++, "Jigme Namgyel").then(async (tx) => {
            return s.addStudent(studentCid++, "Rigden Yoesel").then(async (tx) => {
            return s.addStudent(studentCid++, "Sonam Tshering").then(async (tx) => {
            return s.addStudent(studentCid++, "Tshering Dorji").then(async (tx) => {
            return s.addStudent(studentCid++, "Bijay Kumar Rai").then(async (tx) => {
            return s.addStudent(studentCid++, "Chador Wangdi").then(async (tx) => {
            return s.studentsCount().then(async (count) => {
            assert.equal(count, 7)
     
            return s.searchStudent(5).then(async (student) => {
                assert.equal(student.name, "Tshering Dorji")
            })
            })
            })
            })
            })
            })
            })
            })
        })
    })

     //Testing changing content in the contract
    it('Test mark graduate students', async () => {
        return StudentRecord.deployed().then(async (instance) => {
            s = instance;
            return s.searchStudent(1).then(async (ostudent) => {
            assert.equal(ostudent.name, "Pema Yangzom")
            assert.equal(ostudent.graduated, false)
            return s.markGraduated(1).then(async (transaction) => {
                return s.searchStudent(1).then(async (nstudent) => {
                assert.equal(nstudent.name, "Pema Yangzom")
                assert.equal(nstudent.graduated, true)
                return
                })
            })
            })
        })
    })

    

})
//This function check if the address is valid
function isValidAddress(address){
  assert.notEqual(address, 0x0)
  assert.notEqual(address, '')
  assert.notEqual(address, null)
  assert.notEqual(address, undefined)
}
